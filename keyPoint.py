# -*- coding: utf-8 -*-
import Tkinter
import tkFileDialog
import Func
import cv2
import numpy as np


class NButton(Tkinter.Button):
    def __init__(self, *args, **kw):
        Tkinter.Button.__init__(self, *args, width=15, height=2, **kw)


class Main(Tkinter.Tk):
    def __init__(self):
        Tkinter.Tk.__init__(self)

        self.img1 = cv2.imread('home.png', 0)
        self.img2 = cv2.imread('home_2.png', 0)

        self.cascade = cv2.CascadeClassifier("data/haarcascades/haarcascade_frontalface_alt.xml")
        self.nested = cv2.CascadeClassifier("data/haarcascades/haarcascade_eye.xml")

        self.face_coord_first = {'x': 0, 'y': 0, 'w': 0, 'h': 0, 'detect': False}
        self.face_coord_second = {'x': 0, 'y': 0, 'w': 0, 'h': 0, 'detect': False}
        self.web_cam_coord = {'x': 0, 'y': 0, 'w': 0, 'h': 0, 'detect': False}

        NButton(self, text="Образец 1", command=self.open_first_image).grid(row=1, column=0)
        NButton(self, text="Образец 2", command=self.open_second_image).grid(row=1, column=1)
        NButton(self, text='Вебка', command=self.web_cam_detect).grid(row=1, column=2)
        NButton(self, text="ORB", command=self.orb).grid(row=2, column=0)
        NButton(self, text="SIFT", command=self.sift).grid(row=2, column=1)
        NButton(self, text="FLANN", command=self.flan).grid(row=2, column=2)

    def open_first_image(self):
        self.img1 = self.open_dialog()

    def open_second_image(self):
        self.img2 = self.open_dialog()

    def open_dialog(self):
        path = tkFileDialog.askopenfilename()
        if len(path) == 0:
            return
        image = cv2.imread(path.encode('cp1251'), 0)
        image = Func.histogram_equalization(image)
        face_coord = Func.detect(image, self.cascade)
        image = np.asarray(image)
        if face_coord['detect']:
            print "Обнаружено лицо"
            image = self.cut_face(image, face_coord)
        else:
            print "Лица не обнаружено"
        return image

    def web_cam_detect(self):
        cam = cv2.VideoCapture(0)

        while True:
            _, image = cam.read()
            web_cam_coord = Func.detect(image, self.cascade)
            image = np.array(image)
            if web_cam_coord['detect']:
                img3 = self.cut_face(image, web_cam_coord)
                img3 = cv2.cvtColor(img3, cv2.COLOR_BGR2GRAY)
                if self.face_coord_first['detect']:
                    image = Func.camera_orb(self.img1, img3, image)
                    # image = Func.camera_sift(img1, img3, image)
                    # image = Func.camera_flan(img1, img3, image)
                elif self.face_coord_second['detect']:
                    image = Func.camera_orb(self.img2, img3, image)
                    # image = Func.camera_sift(img2, img3, image)
                    # image = Func.camera_flan(img2, img3, image)
            cv2.imshow('img', image)
            ch = cv2.waitKey(33)
            if ch == 27:
                cv2.destroyAllWindows()
                break
            if ch == 32:
                sa = tkFileDialog.asksaveasfilename(defaultextension='.png')
                if len(sa) > 0:
                    cv2.imwrite(sa.encode('cp1251'), image)

    def cut_face(self, capture, coord):
        face = capture[coord['y']: coord['y'] + coord['h'], coord['x']: coord['x'] + coord['w']]
        return face

    # ORB
    def orb(self):
        Func.orb(self.img1, self.img2)

    # SIFT
    def sift(self):
        Func.sift(self.img1, self.img2)

    # FLAN
    def flan(self):
        Func.flan(self.img1, self.img2)


Main().mainloop()
