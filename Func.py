# -*- coding: utf-8 -*-
import cv2
import numpy as np
from matplotlib import pyplot as plt


def detect(img, cascade):
    coord = {'x': 0, 'y': 0, 'w': 0, 'h': 0, 'detect': False}
    rects = cascade.detectMultiScale(img, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30),
                                     flags=cv2.CASCADE_SCALE_IMAGE)
    if len(rects) == 0:
        return coord
    coord['detect'] = True
    for x1, y1, x2, y2 in rects:
        coord['x'] = x1
        coord['y'] = y1
        coord['w'] = x2
        coord['h'] = y2
    return coord


def draw_matches(img1, kp1, img2, kp2, matches):
    rows1 = img1.shape[0]
    cols1 = img1.shape[1]
    rows2 = img2.shape[0]
    cols2 = img2.shape[1]
    out = np.zeros((max([rows1, rows2]), cols1 + cols2, 3), dtype='uint8')
    out[:rows1, :cols1] = np.dstack([img1, img1, img1])
    out[:rows2, cols1:] = np.dstack([img2, img2, img2])
    for mat in matches:
        img1_idx = mat.queryIdx
        img2_idx = mat.trainIdx
        (x1, y1) = kp1[img1_idx].pt
        (x2, y2) = kp2[img2_idx].pt
        cv2.circle(out, (int(x1), int(y1)), 4, (255, 0, 0), 1)
        cv2.circle(out, (int(x2) + cols1, int(y2)), 4, (255, 0, 0), 1)
        cv2.line(out, (int(x1), int(y1)), (int(x2) + cols1, int(y2)), (255, 0, 0), 1)
    return out


def draw_matches_knn(img1, kp1, img2, kp2, matches):
    rows1 = img1.shape[0]
    cols1 = img1.shape[1]
    rows2 = img2.shape[0]
    cols2 = img2.shape[1]
    out = np.zeros((max([rows1, rows2]), cols1 + cols2, 3), dtype='uint8')
    out[:rows1, :cols1] = np.dstack([img1, img1, img1])
    out[:rows2, cols1:] = np.dstack([img2, img2, img2])
    for m in matches:
        for mat in m:
            img1_idx = mat.queryIdx
            img2_idx = mat.trainIdx
            (x1, y1) = kp1[img1_idx].pt
            (x2, y2) = kp2[img2_idx].pt
            cv2.circle(out, (int(x1), int(y1)), 4, (255, 0, 0), 1)
            cv2.circle(out, (int(x2) + cols1, int(y2)), 4, (255, 0, 0), 1)
            cv2.line(out, (int(x1), int(y1)), (int(x2) + cols1, int(y2)), (255, 0, 0), 1)
    return out


# ORB
def orb(img1, img2):
    orb = cv2.ORB()
    kp1, des1 = orb.detectAndCompute(img1, None)
    kp2, des2 = orb.detectAndCompute(img2, None)
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1, des2)
    matches = sorted(matches, key=lambda x: x.distance)
    print(u'----------------------------------------')
    print(u'Метод ORB:')
    print(u'----------------------------------------')
    print(u'Кол-во совпадений: ' + str(len(matches)))

    good = []
    for m in matches:
        if m.distance < 50:
            good.append(m)
    print(u'Качество совпадения: ' + str(len(good)))

    percent = percent_matches(matches, good)
    print(u'Процент совпадения: ' + str(percent))
    print(u'----------------------------------------')

    img3 = draw_matches(img1, kp1, img2, kp2, good[:])
    plt.imshow(img3), plt.show()


# SIFT
def sift(img1, img2):
    sift = cv2.SIFT()
    kp1, des1 = sift.detectAndCompute(img1, None)
    kp2, des2 = sift.detectAndCompute(img2, None)
    bf = cv2.BFMatcher()

    matches = bf.match(des1, des2)
    print(u'----------------------------------------')
    print(u'Метод SIFT:')
    print(u'----------------------------------------')
    print(u'Кол-во совпадений: ' + str(len(matches)))

    good_matches = bf.knnMatch(des1, des2, k=2)
    good = []
    for m, n in good_matches:
        if m.distance < 0.75 * n.distance:
            good.append([m])
    print(u'Качество совпадения: ' + str(len(good)))

    percent = percent_matches(matches, good)
    print(u'Процент совпадения: ' + str(percent))
    print(u'----------------------------------------')

    img3 = draw_matches_knn(img1, kp1, img2, kp2, good[:])
    plt.imshow(img3), plt.show()


# FLAN
def flan(img1, img2):
    sift = cv2.SIFT()
    kp1, des1 = sift.detectAndCompute(img1, None)
    kp2, des2 = sift.detectAndCompute(img2, None)
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=50)
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.match(des1, des2)
    print(u'----------------------------------------')
    print(u'Метод FLANN:')
    print(u'----------------------------------------')
    print(u'Кол-во совпадений: ' + str(len(matches)))

    good_matches = flann.knnMatch(des1, des2, k=2)
    good = []
    for m, n in good_matches:
        if m.distance < 0.75 * n.distance:
            good.append([m])
    print(u'Качество совпадения: ' + str(len(good)))

    percent = percent_matches(matches, good)
    print(u'Процент совпадения: ' + str(percent))
    print(u'----------------------------------------')

    img3 = draw_matches_knn(img1, kp1, img2, kp2, good[:])
    plt.imshow(img3, ), plt.show()


# ORB camera
def camera_orb(img1, img2, original_image):
    orb = cv2.ORB()
    kp1, des1 = orb.detectAndCompute(img1, None)
    kp2, des2 = orb.detectAndCompute(img2, None)
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1, des2)
    matches = sorted(matches, key=lambda x: x.distance)

    good = []
    for m in matches:
        if m.distance < 50:
            good.append(m)

    percent = percent_matches(matches, good)

    stroke = 'ORB: ' + 'Count: ' + str(len(matches)) + '; ' + 'Quality: ' + str(
        len(good)) + '; ' + 'Percent: ' + str(percent) + ';'

    # img3 = draw_matches(img1, kp1, img2, kp2, good[:])
    pos = (10, 100)
    img3 = draw_text(original_image, stroke, pos)
    return img3


# SIFT camera
def camera_sift(img1, img2, original_image):
    sift = cv2.SIFT()
    kp1, des1 = sift.detectAndCompute(img1, None)
    kp2, des2 = sift.detectAndCompute(img2, None)
    bf = cv2.BFMatcher()

    matches = bf.match(des1, des2)

    good_matches = bf.knnMatch(des1, des2, k=2)
    good = []
    for m, n in good_matches:
        if m.distance < 0.75 * n.distance:
            good.append([m])

    percent = percent_matches(matches, good)

    stroke = 'ORB: ' + 'Count: ' + str(len(matches)) + '; ' + 'Quality: ' + str(
        len(good)) + '; ' + 'Percent: ' + str(percent) + ';'
    # img3 = draw_matches_knn(img1, kp1, img2, kp2, good[:])
    pos = (10, 200)
    img3 = draw_text(original_image, stroke, pos)
    return img3


# FLAN camera
def camera_flan(img1, img2, original_image):
    sift = cv2.SIFT()
    kp1, des1 = sift.detectAndCompute(img1, None)
    kp2, des2 = sift.detectAndCompute(img2, None)
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=50)
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.match(des1, des2)

    good_matches = flann.knnMatch(des1, des2, k=2)
    good = []
    for m, n in good_matches:
        if m.distance < 0.75 * n.distance:
            good.append([m])

    percent = percent_matches(matches, good)

    stroke = 'ORB: ' + 'Count: ' + str(len(matches)) + '; ' + 'Quality: ' + str(
        len(good)) + '; ' + 'Percent: ' + str(percent) + ';'
    # img3 = draw_matches_knn(img1, kp1, img2, kp2, good[:])
    pos = (10, 300)
    img3 = draw_text(original_image, stroke, pos)
    return img3


def percent_matches(matches, good):
    if len(matches) == 0 or len(good) == 0:
        proc = 0
    else:
        proc = len(matches) / len(good)
        proc = 100 / proc
    return proc


def draw_text(img, str, pos):
    font = cv2.FONT_HERSHEY_SIMPLEX
    scale = 0.5
    color = (255, 255, 255)
    thickness = 1
    line_type = 16
    cv2.putText(img, str, pos, font, scale, color, thickness, line_type)
    return img


def histogram_equalization(img):
    # equ = cv2.equalizeHist(img)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    cl1 = clahe.apply(img)
    # res = np.hstack((equ, cl1))

    cv2.imshow('Img', cl1)
    return cl1
