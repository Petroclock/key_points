# keyPoints.py  
Python 2.7  
OpenCV 2.4.13  
NumPy 1.12  
MatPlotlib 1.5.1  
  
Используется графический интерфейс Tkinter.  
Программа использует библиотеку OpenCV для сравнивания двух изображений по ключевым точкам, используя три метода: ORB, SIFT, FLANN.  
Результат выводится через консоль: Кол-во совпадений, Качество совпадения, Процент совпадения  
Имеется функционал сравнивания статичного изображения со съёмкой через веб камеру.  
![figure_1.png](https://bitbucket.org/repo/gjEqGb/images/103662149-figure_1.png)  
![figure_1-1.png](https://bitbucket.org/repo/gjEqGb/images/2390899251-figure_1-1.png)  
![figure_1-2.png](https://bitbucket.org/repo/gjEqGb/images/1041986465-figure_1-2.png)  
![2017-04-02_10-42-37.png](https://bitbucket.org/repo/gjEqGb/images/1556940463-2017-04-02_10-42-37.png)